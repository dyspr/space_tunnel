var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var stars = []
var numOfStars = 3

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < stars.length; i++) {
    stars[i].display()
    stars[i].move()

    if (stars[i].isOffScreen()) {
      stars.splice(i, 1)
    }
  }

  makeStar()

  // masking
  fill(colors.light)
  noStroke()
  rect(windowWidth * 0.5, (windowHeight - boardSize) * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect(windowWidth * 0.5, windowHeight * 0.75 + boardSize * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect((windowWidth - boardSize) * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
  rect(windowWidth * 0.75 + boardSize * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
}

function makeStar() {
  for (var i = 0; i < numOfStars; i++) {
    stars.push(new Star())
  }
}

class Star {
  constructor() {
    this.x = windowWidth * 0.5
    this.y = windowHeight * 0.5
    this.size = 0.001 + Math.random() * 0.001
    this.growth = 0.0001
    this.dir = Math.random() * 2
    this.randS = Math.random() * 0.005
    this.speed = 0.005 + this.randS
    this.acc = 1.00 + this.randS * 16
    this.len = 0
    this.lenGrowth = this.randS
    this.col = 255
  }

  display() {
    noFill()
    stroke(this.col)
    strokeWeight(boardSize * this.size)
    strokeCap(ROUND)
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    push()
    rotate(tan(sin(frameCount * 0.01)))
    push()
    translate(-windowWidth * 0.5, -windowHeight * 0.5)
    line(this.x, this.y, this.x - this.len * sin(this.dir * Math.PI) * boardSize, this.y - this.len * cos(this.dir * Math.PI) * boardSize)
    pop()
    pop()
    pop()
  }

  move() {
    this.speed *= this.acc
    this.x += this.speed * sin(this.dir * Math.PI) * boardSize
    this.y += this.speed * cos(this.dir * Math.PI) * boardSize
    this.size += this.growth
    this.lenGrowth *= this.acc
    this.len += this.lenGrowth
  }

  isOffScreen() {
    if (this.x > windowWidth * 0.5 + boardSize * 1.00 || this.x < windowWidth * 0.5 - boardSize * 1.00 || this.y > windowHeight * 0.5 + boardSize * 1.00 || this.y < windowHeight * 0.5 - boardSize * 1.00) {
      return true
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
